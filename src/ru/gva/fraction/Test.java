package ru.gva.fraction;

import java.util.Scanner;
import java.util.regex.*;

/**
 * @author Gavrikov Vadim 15OIT18.
 */
public class Test {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String[] strings;
        Fraction fraction = new Fraction();
        Fraction fraction1 = new Fraction();
        String str = input();
        strings = splitString(str);
        String[] strings1 = splitarray(strings[0]);
        saveFraction(strings1, fraction);
        strings1 = splitarray(strings[2]);
        saveFraction(strings1, fraction1);
        System.out.println(math(fraction, fraction1, strings[1]));


    }

    /**
     * Метод преобразует элементы строчного массива и сохраняет их в обьект типа Fraction.
     *
     * @param strings1 массив
     * @param fraction обьект
     */
    private static void saveFraction(String[] strings1, Fraction fraction) {
        fraction.setNum(Integer.parseInt(strings1[0]));
        fraction.setDenum(Integer.parseInt(strings1[1]));
    }

    /**
     * Метод разделяет строку на части по пробелам.
     *
     * @param str строка
     * @return разделеная строка по пробелам.
     */
    private static String[] splitString(String str) {
        String[] strings = str.split(" ");
        return strings;
    }

    /**
     * Метод,в котором вводится строка и который разделяет её на 3 части
     * и отправляет в другой метод для сохранения их в массив.
     *
     * @return
     */
    private static String input() {
        String string;
        boolean checkString;
        do {
            string = sc.nextLine();
            checkString = isCheck(string);
            if (!checkString) {
                System.out.println("Некорректно введены данные");
            }
        } while (!checkString);
        return string;
    }

    /**
     * Метод проверяет строку на корректность ввода.
     *
     * @param string строка
     * @return результат проверки
     */
    private static boolean isCheck(String string) {
        Pattern pattern = Pattern.compile("[-]*[1-9][0-9]*/[-]*[1-9][0-9]*\\s[*:+-]\\s[-]*[1-9][0-9]*/[-]*[1-9][0-9]*");
        Matcher matcher = pattern.matcher(string);
        return matcher.find();
    }


    /**
     * Метод, который разделяет строку на числитель и знаменатель.
     *
     * @param string строка
     * @return разделеную строку.
     */
    private static String[] splitarray(String string) {
        String[] strings = string.split("/");
        return strings;
    }

    /**
     * Метод вызывает другие методы в зависимости от действия хранящегося в строке string,
     * для выполнения арифметических действий над 1 и 2 обьектом.
     *
     * @param fraction  1 обьект
     * @param fraction1 2 обьект
     * @param string    дейсвие
     * @return результат вычисления 1 и 2 обьекта.
     */
    public static Fraction math(Fraction fraction, Fraction fraction1, String string) {
        switch (string) {
            case "+":
                return fraction.summa(fraction1);
            case "-":
                return fraction.difference(fraction1);
            case ":":
                return fraction.div(fraction1);
            case "*":
                return fraction.mult(fraction1);
        }
        return null;
    }


}

