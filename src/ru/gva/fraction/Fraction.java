package ru.gva.fraction;


/**
 * @author Gavrikov Vadim 15OIT18.
 */
public class Fraction {
    private int num;
    private int denum;

    public Fraction(int num, int denum) {
        this.num = num;
        this.denum = denum;
    }

    public Fraction(int num) {
        this(num, 1);
    }

    public Fraction() {
        this(1, 1);
    }


    @Override
    public String toString() {
        return num + "/" + denum;
    }


    public void setNum(int num) {
        this.num = num;
    }

    public void setDenum(int denum) {
        this.denum = denum;
    }


    public Fraction summa(Fraction fraction1) {
        Fraction fraction2 = new Fraction();
        if (this.denum == fraction1.denum) {
            fraction2.denum = this.denum;
            fraction2.num = this.num + fraction1.num;
        } else {
            fraction2.denum = this.denum * fraction1.denum;
            fraction2.num = this.num * fraction1.denum + fraction1.num * this.denum;
        }
        return fraction2.reduction();
    }

    public Fraction difference(Fraction fraction1) {
        Fraction fraction2 = new Fraction();
        if (this.denum == fraction1.denum) {
            fraction2.denum = this.denum;
            fraction2.num = this.num - fraction1.num;
        } else {
            fraction2.denum = this.denum * fraction1.denum;
            fraction2.num = this.num * fraction1.denum - fraction1.num * this.denum;
        }
        return fraction2.reduction();
    }

    public Fraction div(Fraction fraction1) {
        Fraction fraction = new Fraction(this.num * fraction1.denum, this.denum * fraction1.num);
        return fraction.reduction();
    }

    public Fraction mult(Fraction fraction2) {
        Fraction fraction3 = new Fraction(this.num * fraction2.num, this.denum * fraction2.denum);
        return fraction3.reduction();

    }

    /**
     * Метод сокращяет дробь.
     *
     * @return в зависимости от возможности сокращение если дробь можно скратить то сокращяет и возвращяет, если нет то исходный вариант.
     */
    public Fraction reduction() {

        if (this.num < 0 && this.denum < 0) {
            this.denum = -this.denum;
            this.num = -this.num;
        }
        if (this.num % this.denum == 0 || this.denum % this.num == 0) {
            for (int i = 2; i < this.denum; i++) {
                if (this.denum / this.num == i && this.denum % this.num == 0) {
                    return new Fraction(1, i);
                }
            }
            for (int i = 1; i < this.num; i++) {
                if (this.num / this.denum == i) {
                    return new Fraction(i, 1);
                }
            }
        } else if (this.num % this.denum != 0 || this.denum % this.num != 0) {
            Fraction fraction = new Fraction();
            for (int i = 2; i < this.denum; i++) {
                if (this.num % i == 0 && this.denum % i == 0) {
                    fraction.num = this.num / i;
                    fraction.denum = this.denum / i;
                    return fraction;
                }

            }
        }

        return new Fraction(this.num, this.denum);
    }


}



